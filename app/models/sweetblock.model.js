const sql = require("./db.js");

// constructor
const SweetBlock = function(user) {
  this.name = user.name;
  this.playername = user.playername;
  this.password = user.password;
  this.email = user.email;
  this.phone = user.phone;
  this.age = user.age;
};

SweetBlock.create = (newSweetBlock, result) => {
  sql.query("INSERT INTO usertable SET ?", newSweetBlock, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created user: ", { id: res.insertId, ...newSweetBlock });
    result(null, { id: res.insertId, ...newSweetBlock });
  });
};

SweetBlock.findById = (id, result) => {
  sql.query(`SELECT * FROM usertable WHERE id = ${id}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found User with the id
    result({ kind: "not_found" }, null);
  });
};

SweetBlock.getAll = (title, result) => {
  let query = "SELECT * FROM usertable";

  if (title) {
    query += ` WHERE title LIKE '%${title}%'`;
  }

  sql.query(query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("users: ", res);
    result(null, res);
  });
};

SweetBlock.updateById = (id, user, result) => {
  sql.query(
    "UPDATE usertable SET name = ?, playername = ?, password = ?, email = ?, phone = ?, age = ? WHERE id = ?",
    [user.name, user.playername, user.password, user.email, user.phone, user.age, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found User with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated users: ", { id: id, ...user });
      result(null, { id: id, ...user });
    }
  );
};

SweetBlock.remove = (id, result) => {
  sql.query("DELETE FROM usertable WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found User with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

SweetBlock.removeAll = result => {
  sql.query("DELETE FROM usertable", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} usertable`);
    result(null, res);
  });
};

module.exports = SweetBlock;
