module.exports = app => {
  const sweetblock = require("../controllers/sweetblock.controller.js");

  var router = require("express").Router();

  // Create a new User
  router.get("/:id", sweetblock.create);

  // Retrieve all Users
  router.get("/findAll", sweetblock.findAll);

  // Retrieve a single User with id
  router.get("/findOne/:id", sweetblock.findOne);

  // Update a User with id
  router.put("/:id", sweetblock.update);

  // Delete a User with id
  router.get("/deleteOne/:id", sweetblock.delete);

  // Delete all Users
  router.get("/deleteAll", sweetblock.deleteAll);

  app.use('/api/sweetblock', router);
};
